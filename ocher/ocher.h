#ifndef OCHER_APP_H
#define OCHER_APP_H

/**
 * @mainpage
 *
 * OcherBook is an open-source ebook reader, supporting multiple file formats and
 * output devices.
 */

#ifndef OCHER_MAJOR
#define OCHER_MAJOR 0
#endif

#ifndef OCHER_MINOR
#define OCHER_MINOR 0
#endif

#ifndef OCHER_PATCH
#define OCHER_PATCH 0
#endif

#endif
